import rospy
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import OccupancyGrid
from matplotlib.pyplot import imshow
from numpy import array,arange,meshgrid,cos,sin
from visualization_msgs.msg import Marker,MarkerArray

skip_laser_entries_divisor = 32 # for efficiency - see code


#%%
# an object to keep our data so we don't have to worry about many global variables

class Data:
    pass

d = Data()


#%%
# Just visualise a bunch of XY points - flatten the incoming arrays in case
# they are not 1D

def visualise_fake_laser_allpoints(X,Y):

    d.marker_array = MarkerArray()
    d.Xflat =  X.flatten()
    d.Yflat = Y.flatten()

    for i in range(len(d.Xflat)):

        marker=Marker()
        marker.header.frame_id = "map"
        marker.lifetime = rospy.Duration(0.5)
        marker.id = i
        marker.type = Marker.SPHERE

        marker.pose.position.x = d.Xflat[i]
        marker.pose.position.y = d.Yflat[i]
        marker.pose.position.z = 0

        marker.scale.x = 0.05
        marker.scale.y = 0.05
        marker.scale.z = 0.05
        marker.color.r = 1.0*i/len(d.Xflat)
        marker.color.g = 1.0*i/len(d.Xflat)
        marker.color.b = 1.0 - 1.0*i/len(d.Xflat)
        marker.color.a = 1.0

        d.marker_array.markers.append(marker)


    d.marker_publisher.publish(d.marker_array)


#%%
# simulate the laser coming out of the robot at robotx,roboty pointing
# in direction theta
# UNFINISHED - just visualises all the possible laser positions
# (note that we sample the laser)

def simulate_laser(robotx,roboty,robotangle,vis=True):

    anglemin = d.lasermsg.angle_min
    anglemax = d.lasermsg.angle_max
    angleinc = d.lasermsg.angle_increment * skip_laser_entries_divisor
    allangles = arange(anglemin,anglemax,
                       angleinc)
    d.allangles_r = allangles + robotangle

    rangemax = d.lasermsg.range_max
#    rangemin = d.lasermsg.range_min
    mapres = d.mapmsg.info.resolution
    d.allranges = arange(0,rangemax,
                       mapres)

    allranges_mesh,allangles_mesh = meshgrid(
                    d.allranges,
                    d.allangles_r,
                    indexing="ij")

    d.Xs = cos(allangles_mesh)*allranges_mesh + robotx
    d.Ys = sin(allangles_mesh)*allranges_mesh + roboty

    if vis:
        visualise_fake_laser_allpoints(d.Xs,d.Ys)


#%%

def lasercb(msg):
    d.lasermsg = msg

    #don't try doing anything if there is no map
    try:
        d.mapmsg
    except:
        return

    for theta in arange(0,2*pi,0.1):
        simulate_laser(0.0,0.0,theta,vis=True)

    print("lasercb")

def mapcb(msg):
    print("mapcb")
    d.mapmsg = msg
    d.maparr = array(d.mapmsg.data).reshape([msg.info.height,msg.info.width])


#%%

rospy.init_node("scannnnnn",disable_signals=True)

d.marker_publisher = rospy.Publisher('/visualization_marker_array',
                                  MarkerArray,
                                  queue_size=10)

rospy.Subscriber("/scan",LaserScan,lasercb,
                 queue_size = 1)

rospy.Subscriber("/map",OccupancyGrid,mapcb,
                 queue_size = 1)

rospy.spin()