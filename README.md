# A step-by-step development of a simple particle filter for robot localisation using laser, for learning.

We are going to use a laser scanner together with a particle filter to localise the robot. The ultimate aim is to publish a transform using TF that links the ``map`` TF reference frame to the robot reference frames.

It is assumed you are already somewhat familiar with what (the most basic form of) a particle filter is and how it works (as will be the case with my BLG456E students when I provide this code).

There are many issues with this code, trade-offs between simplicity and efficiency.
Please do not treat this as state-of-the-art or a reference implementation.
It is designed to introduce particle filters while exploring the capabilities of ROS.

Tested on ROS Kinetic with the Turtlebot and Turtlebot simulation packages installed.

## Running the simulation

The launch file ``sim.launch`` will launch:

- The Gazebo simulation of the Turtlebot which also publishes odometry information between the ``odom`` and ``base_footprint`` frames.

- The "fake laser" of the Turtlebot providing a laser scan on the ``/scan`` topic.

- The "robot state publisher" publishing transforms for the internal state of the robot.

- A map server publishing a pre-calculated occupancy grid map of the environment to the ``/map`` topic.

- An RVIZ instance with a configuration set up to show the environment from the perspective of the ``map`` reference frame (it will therefore show the map but not anything linked to the robot because at this stage there is no transform from the ``map`` reference frame to anything else - this is the job of the localisation program which we will write).

- An RVIZ instance with a configuration set up to show the environment from the perspective of the robot frame of reference (it will therefore not show the map but can show the laser and the robot model).

So run the launch file:

    roslaunch pf_laser_example_456 sim.launch

Now we are ready to try and write a localiser program.

## Step 1: Simulate a laser

Check out the file ``pf1.py``. There is no particle filter there, but it does start the work on one important component. It has gone some way to simulating a laser scan (because in order to calculate the score/weight/likelihood/probability of a hypothesis/particle we need to simulate the laser scan that we would see if the hypothesis were correct and compare it to the actual received laser scan).

The components of the file are as follows:

- ``d``: This is an object for receiving all global variables (so that we do not need to extensively use the ``global`` Python keyword). Note that in Python, you do not need to declare the members of classes before you can use them.

- ``visualise_fake_laser_allpoints``: Will take an array of X and Y points and publish them in the ``map`` reference frame as "markers" that will be shown by RVIZ.

- ``simulate_laser``: Takes a hypothesised x, y and theta coordinate and creates a set of points on the map where the laser may potentially scan (it does not use the map yet to properly simulate the laser, just calculates the points that the laser will be scanning). It sends these points to ``visualise_fake_laser_allpoints`` to visualise them.

- ``lasercb``: Is the function called by ROS with a laser message whenever a new laser range scan message arrives. Note that unless a map message has already been received it does not attempt to do anything. It simply attempts to simulate a set of laser scans at the position 0,0 and a list of different orientations, using the function ``simulate_laser``.

- ``mapcb``: Is the function called by ROS with a map message whenever a new map message arrives (this is broadcast by the map_server node). It also creates a 2D numpy array of the map so the map can be accessed more easily.

To run this, ensure the simulation is running as mentioned in the section "running the simulation" and run:

    python pf1.py

You should see in the map-based RVIZ the simulated laser scans (not taking into account the map yet). This visualisation is published as a MarkerArray on the ``visualization_marker_array`` topic.

## Step 2: Interact with the map

Check out the file ``pf2.py``. There is still no particle filter there, but it properly simulates a laser scan taking into account the map.

New functions are:

- ``vis_simulated_laser_lines``: Visualises a laser scan using line markers to be visualised with rviz. Makes lines from a robot x,y coordinate to a set of X,Y coordinates which signify the end points of laser scans.

- ``simulate_laser``: This function has been finished. It now goes through for each angle in the laser scan and calculates the point at which it hits an obstacle on the map using ray tracing. The output is an array of laser ranges. It also uses the ``vis_simulated_laser_lines`` to visualise this result.

If you want to see the additions from ``pf1.py`` to ``pf2.py`` run the command:

    diff pf1.py pf2.py

To see this simulation in action (again simulating a rotating robot at location 0,0), ensure the simulation is running as mentioned in the section "running the simulation", and run:

    python pf2.py

## Step 3: Particle creation, resampling, and prediction/moving

Check out the file ``pf3.py``. This adds the particle creation, resampling and prediction part of the particle filter. It does not yet add the reweighting part, which will make use of the code written in the previous section.

The core of this particle filter is the "d.particles" variable which is an array. Each row in the array contains a particle. The first column of the array (index 0) contains the particle weights/probabilities. The second column of the array (index 1) contains the hypothesised X coordinates. The second column of the array (index 2) contains the hypothesised Y coordinates. The third column of the array (index 3) contains the hypothesised theta orientations.

New functions are:

- ``apply_prediction``: Randomly add numbers from a normal distribution to the x,y and theta components of each particle. This is the prediction part of the particle filter (this is also called "dynamics", "perturbation" or "addition of noise").

- ``get_resample``: Randomly sampl (take) from a set of particles a given number of particles by choosing from the original set with particles chosen according to the probability in the first column containing weights/probabilities. This is the resampling step of a particle filter.

- ``get_initial_particles``: Get an initial set of particles clustered around the (0,0) coordinate with orientation 0. It uses the ``get_resample`` function to resample 1 particle into many and then runs ``apply_prediction`` several times to add randomness to all these hypotheses.

- ``vis_particles``: Takes the provided set of particles with the same data structure and turns it into a set of poses (PoseArray message) for visualising in RVIZ on the topic ``/particles``.
- ``lasercb``: This has been updated to, in addition to the previous laser scan simulations,

If you want to see the additions from ``pf2.py`` to ``pf3.py`` run the command:

    diff pf2.py pf3.py

To see this simulation in action (in addition to the previous visualisations, it will create a new set of particles each time a laser scan is received and these will be able to be seen visualised in the map-based RVIZ session), ensure the simulation is running as mentioned in the section "running the simulation", and run:

    python pf3.py


## Step 4: Particle reweighting

Check out the file ``pf4.py``. This adds the particle reweighting (scoring) part of the particle filter.

New functions are:

- ``likelihood``: For a provided robot x,y and theta, compares that to the provided laser message by simulating the expected laser that would be seen if the robot were at the robot x,y and theta on tha map, using the previously discussed ``simulate_laser`` function (see steps 1 and 2). It uses a scoring function (discussed in the slides for the relevant BLG456E lecture) that takes into account, with equal contribution from each, four criteria:
   1. The proportion of actual NAN range values that are NAN in the simulated scan.
   2. The proportion of actual non-NAN range values that are non-NAN in the simulated scan.
   3. The proportion of actual non-NAN range values that are within 0.5m to the corresponding non-NAN values in the simulated scan.
   4. The proportion of actual non-NAN range values that are within 0.03m to the corresponding non-NAN values in the simulated scan.

   (note that the result of this is exponentiated 4 times, to provide a more discriminatory resampling).

- ``apply_reweight``: Uses the ``likelihood`` function to calculate weights for all the particles/hypotheses.

- ``lasercb``: This now implements the full particle filter with reweighting, resampling, and prediction phases. Particles are initialised if they don't already exist. The particles are visualised after resampling.

If you want to see the additions from ``pf3.py`` to ``pf4.py`` run the command:

    diff pf3.py pf4.py

To see this particle filter in action, ensure the simulation is running as mentioned in the section "running the simulation", and run:

    python pf4.py


## Step 5: Publishing the resulting TF

Check out the file ``pf5.py``. This averages the set of particles and publishes the result as a TF transform.

New functions are:

- ``send_tf_pose_estimate``: For a provided x, y and theta (which will come from averaging the particles), this function publishes them as a TF transform. The provided pose encapsulates the mapping from ``map`` to ``camera_link`` frames but the part needed by ROS is the transfrom from ``map`` to ``odom`` so the already known transform from ``odom`` to ``camera_link`` is first deducted.

- ``lasercb``: Uses the ``send_tf_pose_estimate`` function to send this TF frame to any other nodes.

If you want to see the additions from ``pf4.py`` to ``pf5.py`` run the command:

    diff pf4.py pf5.py

To see this particle filter in action, ensure the simulation is running as mentioned in the section "running the simulation", and run:

    python pf4.py

If you now go into the map-only RVIZ session and add the robot model visualisation to it, you can now see the current estimate of the robot pose on the map. Similarly, you can now add the map to the robot-only RVIZ session.

To see the new TF tree, one way is to run:

    rosrun tf view_frames
