
# for particles datastructure
# each row contains a particle
# column 0 is the particle probability(weight)
# column 1 is the x coord
# column 2 is the y coord
# column 3 is the theta orientation

import rospy
from geometry_msgs.msg import Point,PoseArray,Pose
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import OccupancyGrid
from matplotlib.pyplot import imshow
from numpy import array,arange,meshgrid,cos,sin
from numpy import logical_not,logical_and,where,array,isnan,absolute,sum
from visualization_msgs.msg import Marker,MarkerArray
from numpy.random import normal,choice
from numpy import empty,nan,dot
from math import pi,atan2
from tf import transformations, TransformBroadcaster, TransformListener

skip_laser_entries_divisor = 32 # for efficiency - see code

xnoisedev = 0.1
ynoisedev = 0.1
thetanoisedev = 0.25

numparticles = 200

epsilon1 = 0.5
epsilon2 = 0.03

# an object to keep our data so we don't have to worry about many global variables

class Data:
    pass

d = Data()


#%%

# Perturb/jitter/randomly move the particles
def apply_prediction(particles): #in-place
    particles[:,1] = normal(particles[:,1],xnoisedev)
    particles[:,2] = normal(particles[:,2],ynoisedev)
    particles[:,3] = normal(particles[:,3],thetanoisedev)

# Sample a new set of particles from first according to probabilities
def get_resample(particles,num_to_get):
    # choice is a numpy function to randomly sample according to the probabilities
    # in the last argument
    new_particles_inds = choice(len(particles),num_to_get,p=particles[:,0].flatten())
    new_particles = particles[new_particles_inds,:]

    new_particles[:,0] = 1.0/num_to_get # ensure adds to 1, equal probs

    return new_particles

# just take one particle and resample it as many times as needed
def get_initial_particles(num_to_get):
    particles = get_resample(array([[1.0,0.0,0.0,0.0]]),num_to_get)
    # now add some noise (do it a few times so the intial estimate is wide enough)
    for i in range(30):
        apply_prediction(particles)

    return particles

# for a single pose estimate / particle / hypothesis calculate its likelihood score
def likelihood(x,y,theta,laser_msg):

    expected = simulate_laser(x,y,theta,vis=False)
    actual = array(laser_msg.ranges[::skip_laser_entries_divisor])

    actualnan = isnan(actual)
    expectednan = isnan(expected)
    actualnum = logical_not(actualnan)
    expectednum = logical_not(expectednan)

    bothnan = logical_and (expectednan , actualnan)
    bothnum = logical_and (expectednum , actualnum)

    nanscore = float(bothnan.sum())/actualnan.sum()
    numscore = float(bothnum.sum())/actualnum.sum()

    diffs = expected[where(bothnum)] - actual[where(bothnum)]

    withinepsilon1 = absolute(diffs) < epsilon1
    withinepsilon2 = absolute(diffs) < epsilon2

    epsilon1score = float(withinepsilon1.sum()) / logical_not(actualnan).sum()
    epsilon2score = float(withinepsilon2.sum()) / logical_not(actualnan).sum()

    score = nanscore/4 + numscore/4 + epsilon1score/4 + epsilon2score/4

    lh = score**4

#    states = "({0: 2.3f},{1: 2.3f},{2: 1.3f})".format(x,y,theta)
#    term1s = "(1/4)*{0:3d}/{1:3d}".format(bothnan.sum(),actualnan.sum())
#    term2s = "(1/4)*{0:3d}/{1:3d}".format(bothnum.sum(),actualnum.sum())
#    term3s = "(1/4)*{0:3d}/{1:3d}".format(withinepsilon1.sum(),logical_not(actualnan).sum())
#    term4s = "(1/4)*{0:3d}/{1:3d}".format(withinepsilon2.sum(),logical_not(actualnan).sum())
#    scores = "{0:2.5f} --> {1:2.5f}".format(score,lh)
#    print(states+": "+term1s+" + "+term2s+" + "+term3s+" + "+term4s+" == "+scores)

    return lh

# Go through all the particles and calculate their likelihoods
def apply_reweight(particles): #in-place

    for pind in range(len(particles)):
        lh = likelihood(particles[pind,1],particles[pind,2],particles[pind,3],d.lasermsg)
        particles[pind,0] *= lh

    particles[:,0] /= particles[:,0].sum() # ensure adds to 1

def average_particles(particles):


    x = dot(particles[:,0]/sum(particles[:,0]),particles[:,1])
    y = dot(particles[:,0]/sum(particles[:,0]),particles[:,2])

    # Averaging angles is a little bit more involved.
    # My approach is to add all the direction vectors and get their average
    # and normalise

    dirx=0
    diry=0
    for theta in particles[:,3]:
        dirx += cos(theta)
        diry += sin(theta)

    theta = atan2(diry,dirx)

    return x,y,theta


#%%
# Just visualise a bunch of XY points - flatten the incoming arrays in case
# they are not 1D
def visualise_fake_laser_allpoints(X,Y):

    d.marker_array = MarkerArray()
    d.Xflat =  X.flatten()
    d.Yflat = Y.flatten()

    for i in range(len(d.Xflat)):

        marker=Marker()
        marker.header.frame_id = "map"
        marker.lifetime = rospy.Duration(0.5)
        marker.id = i
        marker.type = Marker.SPHERE

        marker.pose.position.x = d.Xflat[i]
        marker.pose.position.y = d.Yflat[i]
        marker.pose.position.z = 0

        marker.scale.x = 0.05
        marker.scale.y = 0.05
        marker.scale.z = 0.05
        marker.color.r = 1.0*i/len(d.Xflat)
        marker.color.g = 1.0*i/len(d.Xflat)
        marker.color.b = 1.0 - 1.0*i/len(d.Xflat)
        marker.color.a = 1.0

        d.marker_array.markers.append(marker)


    d.marker_publisher.publish(d.marker_array)


# these X Y points are to be shown as lines from the robot location
def vis_simulated_laser_lines(X,Y,robotx,roboty):

    d.marker_array=MarkerArray()
    assert(len(X)==len(Y))

    for ind in range(len(X)):

      marker=Marker()
      marker.header.frame_id="map"
      marker.id=ind
      marker_cntr=ind+1

      p1 = Point()
      p1.x = robotx
      p1.y = roboty

      p2 = Point()
      p2.x = X[ind]
      p2.y = Y[ind]

      marker.points = [p1,p2]

      marker.scale.x=0.05
      marker.scale.y=0.05
      marker.scale.z=0.05
      marker.color.r=1.0
      marker.color.g=(1.0*marker_cntr)/len(X)
      marker.color.b=1.0-(1.0*marker_cntr)/len(X)
      marker.color.a=1.0
      marker.lifetime = rospy.Duration(0.5)

      d.marker_array.markers.append(marker)

    d.marker_publisher.publish(d.marker_array)


# This takes an array of particles (see data structure
# description above) and sends them to rviz to be visualised
def vis_particles(particles):

    d.pose_arr = PoseArray()
    d.pose_arr.header.frame_id="map"

    for particle in particles:

        p = Pose()

        p.position.x=particle[1]
        p.position.y=particle[2]

        qz = transformations.quaternion_about_axis(particle[3], [0,0,1])
        p.orientation.x = qz[0]
        p.orientation.y = qz[1]
        p.orientation.z = qz[2]
        p.orientation.w = qz[3]

        d.pose_arr.poses.append(p)

    d.pose_swarm_publisher.publish(d.pose_arr)


#%%
# simulate the laser coming out of the robot at robotx,roboty pointing
# in direction theta

def simulate_laser(robotx,roboty,robotangle,vis=True):

    anglemin = d.lasermsg.angle_min
    anglemax = d.lasermsg.angle_max
    angleinc = d.lasermsg.angle_increment * skip_laser_entries_divisor
    allangles = arange(anglemin,anglemax,
                       angleinc)
    d.allangles_r = allangles + robotangle

    rangemax = d.lasermsg.range_max
    rangemin = d.lasermsg.range_min
    mapres = d.mapmsg.info.resolution
    d.allranges = arange(0,rangemax,
                       mapres)

    allranges_mesh,allangles_mesh = meshgrid(
                    d.allranges,
                    d.allangles_r,
                    indexing="ij")

    d.Xs = cos(allangles_mesh)*allranges_mesh + robotx
    d.Ys = sin(allangles_mesh)*allranges_mesh + roboty

#    if vis
#        visualise_fake_laser_allpoints(d.Xs,d.Ys)

    maporiginx = d.mapmsg.info.origin.position.x
    maporiginy = d.mapmsg.info.origin.position.y

    mapcolumns = (d.Xs - maporiginx) / mapres
    maprows = (d.Ys - maporiginy) / mapres

    results = empty([len(allangles)])
    results.fill(nan)
    topubX=[]
    topubY=[]

    for angleind in range(len(allangles)):
        for rangeind in range(len(d.allranges)):

            map_col = int(round(mapcolumns[rangeind,angleind]))
            map_row = int(round(maprows[rangeind,angleind]))

            if map_row<d.maparr.shape[0] and map_col<d.maparr.shape[1]:
                occupancy = d.maparr[map_row,map_col]
            else:
                occupancy = -1

            if occupancy > 0:

                this_range = d.allranges[rangeind]
                if this_range < rangemin:
                    results[angleind] = nan
                else:
                    results[angleind] = this_range
                    topubX.append(d.Xs[rangeind,angleind])
                    topubY.append(d.Ys[rangeind,angleind])
                break
    if vis:
        vis_simulated_laser_lines(topubX,topubY,robotx,roboty)

    return results


#%%

# Here we broadcast a transform from the /map frame to the /odom frame
# using the average pose estimate from our particles
# Even though we didn't make use of information from odometry (we should have)
# we need to give the transform to /odom since the whole TF tree already relies
# on /odom.
def send_tf_pose_estimate(x,y,theta):
    try:

        # get the existing transform from /odom to /camera_link
        (transod,rotod) =  d.tf_listener.lookupTransform('/odom', '/camera_link', rospy.Time(0))
        ignored1,ignored2,thetaod = transformations.euler_from_quaternion(rotod)

        # calculate the transform from /map to /odom (we have from /map to /camera_link)
        xdiff = x - transod[0]
        ydiff = y - transod[1]
        thetadiff = theta - thetaod

        d.tf_broadcaster.sendTransform((xdiff, ydiff, 0),
                 transformations.quaternion_from_euler(0, 0, thetadiff),
                 rospy.Time.now(),
                 "odom",
                 "map")

    except Exception as e:
        print("Exception in send_tf_pose_estimate")
        raise e

def lasercb(msg):
    d.lasermsg = msg

    #don't try doing anything if there is no map
    try:
        d.mapmsg
    except:
        return

    try:
        d.particles
    except:
        d.particles = get_initial_particles(numparticles)

    apply_reweight(d.particles)
    d.particles = get_resample(d.particles,numparticles)

    vis_particles(d.particles)

    x,y,theta = average_particles(d.particles)
    send_tf_pose_estimate(x,y,theta)

    apply_prediction(d.particles)

#    for particle in d.particles:
#        expected = simulate_laser(particle[1],particle[2],particle[3],vis=True)

    print("lasercb")

def mapcb(msg):

    print("mapcb")
    d.mapmsg = msg
    d.maparr = array(d.mapmsg.data).reshape([msg.info.height,msg.info.width])


#%%

rospy.init_node("scannnnnn",disable_signals=True)

d.marker_publisher = rospy.Publisher('/visualization_marker_array',
                                  MarkerArray,
                                  queue_size=1)

d.pose_swarm_publisher = rospy.Publisher('/particles',PoseArray,queue_size=1)

d.tf_broadcaster = TransformBroadcaster()

d.tf_listener = TransformListener()

rospy.Subscriber("/scan",LaserScan,lasercb,
                 queue_size = 1)

rospy.Subscriber("/map",OccupancyGrid,mapcb,
                 queue_size = 1)

rospy.spin()